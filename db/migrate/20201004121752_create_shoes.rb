class CreateShoes < ActiveRecord::Migration[6.0]
  def change
    create_table :shoes do |t|
    	t.string :s_pic
    	t.string :s_name
    	t.text	:s_desc
    	t.text :s_prize

      t.timestamps
    end
  end
end
