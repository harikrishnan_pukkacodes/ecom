class CreatePhones < ActiveRecord::Migration[6.0]
  def change
    create_table :phones do |t|
    	t.string :p_pic
    	t.string :p_name
    	t.text	:p_desc
    	t.text :p_prize
      t.timestamps
    end
  end
end
