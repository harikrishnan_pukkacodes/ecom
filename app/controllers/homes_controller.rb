class HomesController < ApplicationController
	before_action :authenticate_admin!
	def index
		@homes = Home.all
	end
end
