class ShoesController < ApplicationController
	def index
		@shoes = Shoe.all
	end
	def new
		@shoe = Shoe.new
	end
	def show
		@shoe = Shoe.find(params[:id])
	end	
	def edit
		@shoe = Shoe.find(params[:id])
	end
	def create
		@shoe = Shoe.create(shoe_params)
  		if @shoe.save
    		redirect_to shoes_path(id: @shoe.id)
  		else
    		render 'new'
  		end
	end
	private
  	def shoe_params
    	params.require(:shoe).permit(:s_pic, :s_name, :s_desc, :s_prize)
  	end

end
