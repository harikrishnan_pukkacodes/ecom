class PhonesController < ApplicationController
	def index
		@phones = Phone.all
	end
	def new
		@phone = Phone.new
	end
	def show
		@phone = Phone.find(params[:id])
	end	
	def edit
		@phone = Phone.find(params[:id])
	end
	def create
		@phone = Phone.create(phone_params)
  		if @phone.save
    		redirect_to phones_path(id: @phone.id)
  		else
    		render 'new'
  		end
	end
	private
  	def phone_params
    	params.require(:phone).permit(:p_pic, :p_name, :p_desc, :p_prize)
  	end
end
