Rails.application.routes.draw do
  devise_for :admins do
  	resources :homes
  end
  resources :phones, :shoes
  
  root 'homes#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
